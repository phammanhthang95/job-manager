package main

import (
	"context"
	"gitlab.com/phammanhthang95/job-manager"
	"log"
	"time"
)

func main() {
	job1 := job_manager.NewJob(
		func(ctx context.Context) error {
			time.Sleep(time.Second)
			log.Println("I am job 1")

			return nil
			//return errors.New("something went wrong at job 1")
		},
		job_manager.WithName("Something 1"),
		job_manager.WithRetryDuration([]time.Duration{time.Second * 3}),
	)

	job2 := job_manager.NewJob(
		func(ctx context.Context) error {
			time.Sleep(time.Second)
			log.Println("I am job 2")

			return nil
		},
		job_manager.WithName("Something 1"),
		job_manager.WithRetryDuration([]time.Duration{time.Second * 3}),
	)

	job3 := job_manager.NewJob(
		func(ctx context.Context) error {
			time.Sleep(time.Second)
			log.Println("I am job 3")

			return nil
		},
		job_manager.WithName("Something 1"),
		job_manager.WithRetryDuration([]time.Duration{time.Second * 3}),
	)

	group := job_manager.NewGroup(true, job1, job2, job3)

	if err := group.Run(context.Background()); err != nil {
		log.Println(err)
	}
}