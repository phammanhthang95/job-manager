# JobManager
Helper for create job manager base.

### Install

``` bash
 go get gitlab.com/phammanhthang95/job-manager
```

### Usage

Library have 2 modules job and job manager, or use can use both.
For example to setup:

* Create New Job

``` go

job1 := job_manager.NewJob(
    func(ctx context.Context) error {
        time.Sleep(time.Second)
        log.Println("I am job 1")

        return nil
        //return errors.New("something went wrong at job 1")
    },
    job_manager.WithName("Something 1"),
    job_manager.WithRetryDuration([]time.Duration{time.Second * 3}),
)

```


* Config Job and ready to execute job


``` go

// param 1: is concurrency
// param 2, 3, 4 ... is per job
group := job_manager.NewGroup(true, job1, job2, job3)


```


* Run Job
``` go

if err := group.Run(context.Background()); err != nil {
    log.Println(err)
}

```

##### In this tutorial, we have executed job in Golang.
